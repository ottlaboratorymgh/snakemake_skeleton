#!/usr/bin/env python3

__author__ = "Sharon S. Wu (sharon.wu@mgh.harvard.edu)"
__version__ = 0.1

"""
Argument parser for snakemake skeleton pipeline.
TODO: handle cluster commands, handle JSON cluster config
"""

import sys, os
from os.path import realpath
import subprocess
import argparse
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, RawDescriptionHelpFormatter, SUPPRESS
import yaml

## Custom parser class (prints out error message when arguments are unparseable)
class ArgumentParserWithHelp(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


def run():
    #############################
    ## Overall parser
    #############################
    desc = """
    SNAKEMAKE SKELETON
    Sharon S. Wu (sharon.wu@mgh.harvard.edu)
    Ott Group @ MGH CCR

    This skeletal snakemake \"pipeline\" serves as a file and directory template
    for snakemake pipelines.

    Available modules:

            +---------------> all +--------------+
            |                                    |
       +------> preproc +---> map +---> filter +------>

    """

    ## Main parser
    parser = ArgumentParserWithHelp(description=desc, formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--version', '-v', action='version',
                        version='%(prog)s ' + str(__version__))

    ## Initializing subparsers
    subparsers = parser.add_subparsers(dest="module")
    subparsers.required = True


    #############################
    ## preproc module parser
    prepdesc = """
    Trims adapter sequences and degenerate bases from input FASTQ reads.
    Also performs quality control checks before and after trimming.
    """
    prepparser = subparsers.add_parser("preproc", description=prepdesc, formatter_class=RawDescriptionHelpFormatter,
                                        help="Trim input reads")
    prepparser.add_argument("-c", "--config", type=str, required=True, dest="configfile", default=SUPPRESS,
                            help="YAML file with paths to sample files and analysis parameters. See 'templates' directory for example.")
    prepparser.add_argument("-a", "--adapters", type=str, required=False, dest="adapterFile", default=SUPPRESS,
                            help="Adapter FASTA file. Overrides the adapter FASTA specified in the config file.")
    prepparser.add_argument("-l", "--length", type=int, required=False, dest="readLength",
                            help="Length of untrimmed reads.")
    prepparser.add_argument("-t", "--threads", type=int, required=False, dest="threads",
                            help="Number of threads to use. When using cluster commands, \
                            this specifies the number of jobs to submit at once. [default = 1]")
    prepparser.add_argument("--dryrun", action='store_true', required=False, dest="dryrun", default=False,
                            help="Activate dry-run mode (for debugging purposes). [default = False]")
    prepparser.add_argument("--rerun", action='store_true', required=False, dest="rerun", default=False,
                            help="Re-run unsuccessfully completed rules (for debugging purposes). [default = False]")


    #############################
    ## Map module parser
    mapdesc = """
    Maps FASTQs (raw or preprocessed) to a reference genome.
    """
    mapparser = subparsers.add_parser("map", description=mapdesc, formatter_class=RawDescriptionHelpFormatter,
                                        help="Map FASTQs")
    mapparser.add_argument("-c", "--config", type=str, required=True, dest="configfile", default=SUPPRESS,
                            help="YAML file with paths to sample files and analysis parameters. See 'templates' directory for example.")
    mapparser.add_argument("-i", "--index", type=str, required=False, dest="indexFile", default=SUPPRESS,
                            help="Path and basename of bowtie2 indices. Overrides the reference specified in the config file.")
    mapparser.add_argument("-p", "--phredScore", type=int, required=False, dest="phred",
                            help="Phred encoding of input FASTQs. Overrides config file parameter. [default = phred33]")
    mapparser.add_argument("-t", "--threads", type=int, required=False, dest="threads",
                            help="Number of threads to use. When using cluster commands, \
                            this specifies the number of jobs to submit at once. [default = 1]")
    mapparser.add_argument("--dryrun", action='store_true', required=False, dest="dryrun", default=False,
                            help="Activate dry-run mode (for debugging purposes). [default = False]")
    mapparser.add_argument("--rerun", action='store_true', required=False, dest="rerun", default=False,
                            help="Re-run unsuccessfully completed rules (for debugging purposes). [default = False]")


    #############################
    ## Filter module parser
    filtdesc = """
    Filters alignments. Flags duplicates (optical & PCR) and discards unmapped reads.
    """
    filterparser = subparsers.add_parser("filter", description=filtdesc, formatter_class=RawDescriptionHelpFormatter,
                                        help="Filter alignments")
    filterparser.add_argument("-c", "--config", type=str, required=True, dest="configfile", default=SUPPRESS,
                                help="YAML file with paths to sample files and analysis parameters. See 'templates' directory for example.")
    filterparser.add_argument("-t", "--threads", type=int, required=False, dest="threads",
                                help="Number of threads to use. When using cluster commands, \
                                this specifies the number of jobs to submit at once. [default = 1]")
    filterparser.add_argument("--dryrun", action='store_true', required=False, dest="dryrun", default=False,
                                help="Activate dry-run mode (for debugging purposes). [default = False]")
    filterparser.add_argument("--rerun", action='store_true', required=False, dest="rerun", default=False,
                            help="Re-run unsuccessfully completed rules (for debugging purposes). [default = False]")



    #############################
    ## Parser for running all modules
    alldesc = """
    Runs entire snakemake skeleton pipeline.

            +---------------> all +--------------+
            |                                    |
       +------> preproc +---> map +---> filter +------>
    """
    allparser = subparsers.add_parser("all", description=alldesc, formatter_class=RawDescriptionHelpFormatter,
                                        help="Run entire SLAM-DUNKAROO pipeline")
    allparser.add_argument("-c", "--config", type=str, required=True, dest="configfile", default=SUPPRESS,
                            help="YAML file with paths to sample files and analysis parameters. See 'templates' directory for example.")
    allparser.add_argument("-a", "--adapters", type=str, required=False, dest="adapterFile", default=SUPPRESS,
                            help="Adapter FASTA file. Overrides the adapter FASTA specified in the config file.")
    allparser.add_argument("-l", "--length", type=int, required=False, dest="readLength",
                            help="Length of untrimmed reads.")
    allparser.add_argument("-i", "--index", type=str, required=False, dest="indexFile", default=SUPPRESS,
                            help="Path and basename of bowtie2 indices. Overrides the reference specified in the config file.")
    allparser.add_argument("-p", "--phredScore", type=int, required=False, dest="phred",
                            help="Phred encoding of input FASTQs. Overrides config file parameter. [default = phred33]")
    allparser.add_argument("-t", "--threads", type=int, required=False, dest="threads",
                            help="Number of threads to use. When using cluster commands, \
                            this specifies the number of jobs to submit at once. [default = 1]")
    allparser.add_argument("--dryrun", action='store_true', required=False, dest="dryrun", default=False,
                            help="Activate dry-run mode (for debugging purposes). [default = False]")
    allparser.add_argument("--rerun", action='store_true', required=False, dest="rerun", default=False,
                            help="Re-run unsuccessfully completed rules (for debugging purposes). [default = False]")

    args = parser.parse_args()


    #############################
    ## Executing appropriate snakemake skeleton workflow.
    ## Also checks command line options with parameters provided in config file.
    #############################
    module = args.module
    configfile = yaml.load(open(args.configfile, "r"))
    snakefile_path = os.path.dirname(os.path.abspath(sys.argv[0]))
    cmd = "snakemake -k -T --use-conda --nolock " + \
            "--cluster \'bsub -J {cluster.name} -q {cluster.queue} -o {cluster.output} -e {cluster.error} -R {cluster.resources} -M {cluster.memory} -n {cluster.nCPUs}\' " + \
            "--cluster-config " + os.path.join(snakefile_path, "helper_scripts/snakemake_skeleton_clusterConfig.json") + \
            "-s " + os.path.join(snakefile_path, "slamdunkaroo.sn") + \
            " --configfile " + args.configfile + \
            " --config module=" + module

    if module == "preproc":
        # check if adapter file provided
        if 'adapterFile' not in args and 'adapters' not in configfile:
            raise ValueError("No adapters provided in command line or config file. " +
                                "Please add path to adapter FASTA file.")
        elif 'adapterFile' in args and args.adapterFile is not None:
            if 'adapters' in configfile:
                print("Adapter file in config overrode; " +
                        "running preprocessing module with adapters: " +
                        args.adapterFile)
            cmd += " adapters=" + args.adapterFile
        # check if length parameter provided
        if args.readLength is None and 'readLength' not in configfile:
            raise ValueError("Missing parameter in command line or config file: readLength")
        elif args.readLength is not None:
            if 'readLength' in configfile:
                print("Parameter 'readLength' in config overrode; " +
                        "running preprocessing module with readLength=" +
                        str(args.readLength))
            cmd += " readLength=" + str(args.readLength)

    elif module == "map":
        # check if bowtie2 index files provided
        if 'indexFile' not in args and 'btIndex' not in configfile:
            raise ValueError("No bowtie2 index files provided in command line or config file. " +
                                "Please add path to bowtie2 index files.")
        elif 'indexFile' in args and args.indexFile is not None:
            if 'btIndex' in configfile:
                print("Bowtie2 index files in config overrode; " +
                        "running mapping module with bowtie2 index: " +
                        args.indexFile)
            cmd += " btIndex=" + args.indexFile
        # check if phred score parameter provided
        if args.phred is None and 'phredScore' not in configfile:
            print("Missing parameter in command line or config file: phredScore")
            print("Running mapping module with default phredScore=phred33.")
            cmd += " phredScore=phred33"
        elif args.phred is not None:
            if 'phredScore' in configfile:
                print("Parameter 'phredScore' in config overrode; " +
                        "running mapping module with phredScore=" +
                        str(args.phred))
            cmd += " phredScore=" + str(args.phred)

    else: # module == "all"
        # check if adapter file provided
        if 'adapterFile' not in args and 'adapters' not in configfile:
            raise ValueError("No adapters provided in command line or config file. " +
                                "Please add path to adapter FASTA file.")
        elif 'adapterFile' in args and args.adapterFile is not None:
            if 'adapters' in configfile:
                print("Adapter file in config overrode; " +
                        "running preprocessing module with adapters: " +
                        args.adapterFile)
            cmd += " adapters=" + args.adapterFile
        # check if length parameter provided
        if args.readLength is None and 'readLength' not in configfile:
            raise ValueError("Missing parameter in command line or config file: readLength")
        elif args.readLength is not None:
            if 'readLength' in configfile:
                print("Parameter 'readLength' in config overrode; " +
                        "running preprocessing module with readLength=" +
                        str(args.readLength))
            cmd += " readLength=" + str(args.readLength)
        # check if bowtie2 index files provided
        if 'indexFile' not in args and 'btIndex' not in configfile:
            raise ValueError("No bowtie2 index files provided in command line or config file. " +
                                "Please add path to bowtie2 index files.")
        elif 'indexFile' in args and args.indexFile is not None:
            if 'btIndex' in configfile:
                print("Bowtie2 index files in config overrode; " +
                        "running mapping module with bowtie2 index: " +
                        args.indexFile)
            cmd += " btIndex=" + args.indexFile
        # check if phred score parameter provided
        if args.phred is None and 'phredScore' not in configfile:
            print("Missing parameter in command line or config file: phredScore")
            print("Running mapping module with default phredScore=phred33.")
            cmd += " phredScore=phred33"
        elif args.phred is not None:
            if 'phredScore' in configfile:
                print("Parameter 'phredScore' in config overrode; " +
                        "running mapping module with phredScore=" +
                        str(args.phred))
            cmd += " phredScore=" + str(args.phred)
        

    # check if threads parameter provided
    if args.threads is None:
        cmd += " --cores 1"
    else: # args.threads is not None:
        cmd += " --cores " + str(args.threads)

    # check if dry-run mode is activated
    if args.dryrun == True:
        cmd += " -n"

    # check if rerun-incomplete mode is activated
    if args.rerun == True:
        cmd += " --ri"
    else:
        cmd += " -F"

    ## Running command and printing snakemake output to stdout in real time
    print(cmd, file=sys.stdout)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
    lines_iterator = iter(p.stdout.readline, b"")
    for line in lines_iterator:
        print(line.strip().decode("utf-8"))


if __name__ == "__main__":
    run()

## END
