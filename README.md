
# Introduction

This repository serves as a skeletal directory and file structure guide for snakemake pipelines employed by the Ott Group @ MGH Center for Cancer Research. This example pipeline is intended as a guide rather than as a working tool, but in theory it should be usable with minimal code changes.

***

# Dependencies

The pipeline requires Conda with Python 3.6+ to be installed ([Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html) is fine).

Once Conda is installed, channel priorities need to be set. Following the instructions [here](https://conda.io/docs/user-guide/tasks/manage-channels.html), add the following channels (in descending priority): `bioconda`, `conda-forge`, `defaults`. 
*NOTE: Recently, bioconda changed their recommended channel priorities to put `conda-forge` at a higher priority than `bioconda`. This pipeline was not written with this change in mind, but should still work with the new recommended channel priorities.*

```
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
```

This pipeline requires the [Snakemake](http://snakemake.readthedocs.io/en/stable/) package from Conda. The pipeline runs on version 4.8.0 (but should work with 5.x).

To create an environment (here, called `snakemake_env`) with Snakemake installed:
```
conda create -n snakemake_env snakemake=4.8.0
```
To install snakemake in an already-existing environment, activate the environment and run:
```
conda install snakemake=4.8.0
```


***

# Installation

Currently the only "installation" option is to clone the repository.

You need to navigate to directory into which the repository was cloned and add the executeable file to your system path:

```
cd snakemake_skeleton
export PATH=$(pwd):$PATH
```

Test to see if the installation was successful:

```
snakemake_skeleton -h
```

If a help message appears, you are set to jet!

***

# Quick Start

**1. Setup experiment config file**

Modify the template configuration file (`templates/Experiment1-SnakeSkeletonConfig.yaml`). File paths to sample FASTQ/gzipped FASTQ files must be provided. Sample names (replacing `SampleA`, etc.) and the experiment name can also be customized. All other parameters have been set to their defaults and do not need to be changed unless desired. Comments in the template file should describe the mandatory-ness of specific parameters.

**2. Run complete analysis with default settings**

Create or navigate to the directory in which you wish to run the analysis (does not need to be the same directory as the sample files) and invoke the `snakemake_skeleton` command. You can run the pipeline with one job at a time:
```
snakemake_skeleton all -c /path/to/config/file.yaml
```
Or you can run multiple jobs simultaneously (for example, 6 jobs at a time):
```
snakemake_skeleton all -c /path/to/config/file.yaml -t 6
```
Before starting the analysis, it is good practice to quickly check that all the files and dependencies are in place by performing a dry-run:
```
snakemake_skeleton all -c /path/to/config/file.yaml --dryrun
```
If anything is missing, an error message should come up describing what needs to be fixed.

**Note: when running in cluster mode, which is default (you need to go into the source code of the parser to change this), the `threads` parameter dictates how many jobs get submitted to the cluster at the same time (keeping rule dependencies in mind).**

***

# Detailed Usage

The following sections explore the modules of the pipeline in depth, including module-specific parameters and input/output files.

### Setup

The pipeline makes use of a configuration file which contains paths to sample files and necessary reference files as well as analysis parameters. A template YAML file (`templates/Experiment1-SnakeSkeletonConfig.yaml`) showing the proper layout of this configuration file has been provided.

It is recommended to use absolute file paths so the working directory can be independent of where the files are located. It is also recommended to specify all analysis parameters in the configuration file directly. The pipeline does accept command line parameter arguments; these will override any values set in the configuration file (if present). If a required parameter is not specified in the configuration file nor command line, the pipeline will error out requesting a valid value for that parameter.

How to set up the project directory is up to personal preference, but a suggested example layout would be:
```
project/
|---config.yaml
|---fastq/
     |---SampleA/
     |    |---SampleA_unmapped.fastq.gz
     |
     |---SampleB/
     |    |---SampleB_unmapped.fastq.gz
    ...

resources/
|---adapters.fa
|---bowtie2_index/
     |---hg19/
          |---hg19.1.bt2
          |---hg19.2.bt2
         ...

```

Once the project directory and configuration file have been prepared, the analysis can be invoked by specifying a particular module (`preproc`, `map`, or `filter`) or running the whole analysis pipeline (`all`). Downstream modules require files output by upstream modules or manually created files following the same naming format.

```
usage: snakemake_skeleton [-h] [--version] {preproc,map,filter,all} ...

    SNAKEMAKE SKELETON
    Sharon S. Wu (sharon.wu@mgh.harvard.edu)
    Ott Group @ MGH CCR

    This skeletal snakemake "pipeline" serves as a file and directory template
    for snakemake pipelines.

    Available modules:

            +---------------> all +--------------+
            |                                    |
       +------> preproc +---> map +---> filter +------>



positional arguments:
  {preproc,map,filter,all}
    preproc             Trim input reads
    map                 Map FASTQs
    filter              Filter alignments
    all                 Run entire SLAM-DUNKAROO pipeline

optional arguments:
  -h, --help            show this help message and exit
  --version, -v         show program's version number and exit
```


### *Preproc* Module

The `preproc` module takes in raw, *single-end* reads (this can be changed as needed to paired-end or both) in the form of gzipped FASTQ files and performs adapter and quality trimming.

First, the module performs FASTQC on the input FASTQ files and outputs the results in a `stats/` directory in the project directory from which `snakemake_skeleton` was invoked. Low quality squencing tails (N's) and adapter sequences (as provided in the user-specified FASTA file) are then trimmed from the input reads. By convention, the trimming tool `skewer` recommends a minimum read length of 35bp after trimming. For reads with lengths greater than 35bp, this convention is used. For reads with lengths less than or equal to 35bp, a minimum read length of 21bp is used. Reads shorter than 21 base pairs generally are not long enough to map uniquely to a reference sequence. FASTQC is then performed again on the trimmed FASTQ files.

Input                                                   | Output
--------------------------------------------------------|-------------------------------------------------------------
`<sample file specified in config>.fastq.gz`            | `/stats/SampleName/SampleName.raw_fastqc/`, `/stats/SampleName/SampleName.trimmed_fastqc/`, `/preproc/SampleName/SampleName-trimmed.fastq.gz`


##### **Command Line Arguments**

Flag                                  | Description
--------------------------------------|-----------------------------------------------------------------------------------------------
-h, \-\-help                          | Displays help message
-c <FILE\>, \-\-config <FILE\>      | YAML configuration file with paths to sample data and resource files (and analysis parameters, recommended but optional). Structure follows that of the template YAML file provided in the SLAMDUNKAROO repository. Required.
-a <FILE\>, \-\-adapters <FILE\>    | Adapter FASTA file. Overrides the adapter FASTA specified in the config file. Required.
-l <INT\>, \-\-length <INT\>        | Length of untrimmed reads. Overrides value specified in the config (if applicable). Required.
-t <INT\>, \-\-threads <INT\>       | Number of threads to use/jobs to run at once. If using a machine with multiple cores, should use values >1 to speed up analysis. Defaults to 1.
\-\-dryrun                            | Activates dryrun mode. Good to check for presence of necessary input files, parameters, and dependencies.
\-\-rerun                             | Activates rerun-incomplete mode. This mode only runs rules that did not complete successfully in prior pipeline runs. 


### *Map* Module

The `map` module takes in *single-end* reads (this can be changed as needed to paired-end or both) in the form of gzipped FASTQ files and outputs a sorted and indexed BAM alignment file. The module is designed to be used with trimmed output from the `preproc` module.

The FASTQs are aligned to the user-provided reference genome using Bowtie2 with very sensitive alignment parameters. The BAM output is sorted and indexed with samtools (v1.8).

Input                                                   | Output
--------------------------------------------------------|-------------------------------------------------------------
`<sample file specified in config>.fastq.gz`            | `/map/SampleName/SampleName.sorted.bam`, `/map/SampleName/SampleName.sorted.bam.bai`


##### **Command Line Arguments**

Flag                                  | Description
--------------------------------------|-----------------------------------------------------------------------------------------------
-h, \-\-help                          | Displays help message
-c <FILE\>, \-\-config <FILE\>      | YAML configuration file with paths to sample data and resource files (and analysis parameters, recommended but optional). Structure follows that of the template YAML file provided in the SLAMDUNKAROO repository. Required.
-i <FILE\>, \-\-index <FILE\>       | Path and basename of bowtie2 index files. Overrides index file path specified in the config (if applicable). Required.
-p <INT\>, \-\-phred <INT\>         | Phred encoding of input FASTQs. Overrides value specified in the config (if applicable). Defaults to phred33 if no value provided in config file or command line.
-t <INT\>, \-\-threads <INT\>       | Number of threads to use/jobs to run at once. If using a machine with multiple cores, should use values >1 to speed up analysis. Defaults to 1.
\-\-dryrun                            | Activates dryrun mode. Good to check for presence of necessary input files, parameters, and dependencies.
\-\-rerun                             | Activates rerun-incomplete mode. This mode only runs rules that did not complete successfully in prior pipeline runs.


### *Filter* Module

The `filter` module takes in a sorted and indexed BAM alignment file, removes unmapped reads, and marks duplicates. The BAM of the fully-filtered alignments is sorted and indexed with samtools (v1.8).

Input                                                   | Output
--------------------------------------------------------|-------------------------------------------------------------
`/map/SampleName/SampleName.sorted.bam`                 | `/filter/SampleName/SampleName.filtered.bam`, `/filter/SampleName/SampleName.filtered.bam.bai`


##### **Command Line Arguments**

Flag                                  | Description
--------------------------------------|-----------------------------------------------------------------------------------------------
-h, \-\-help                          | Displays help message
-c <FILE\>, \-\-config <FILE\>      | YAML configuration file with paths to sample data and resource files (and analysis parameters, recommended but optional). Structure follows that of the template YAML file provided in the SLAMDUNKAROO repository. Required.
-t <INT\>, \-\-threads <INT\>       | Number of threads to use/jobs to run at once. If using a machine with multiple cores, should use values >1 to speed up analysis. Defaults to 1.
\-\-dryrun                            | Activates dryrun mode. Good to check for presence of necessary input files, parameters, and dependencies.
\-\-rerun                             | Activates rerun-incomplete mode. This mode only runs rules that did not complete successfully in prior pipeline runs.


### *All* Module

The `all` module runs the entire analysis, encompassing all the steps of the individual modules outlined above.

Input                                                   | Output
--------------------------------------------------------|-------------------------------------------------------------
`<sample file specified in config>.fastq.gz`            | `/stats/SampleName/SampleName.raw_fastqc/`, `/stats/SampleName/SampleName.trimmed_fastqc/`, `/map/SampleName/SampleName.sorted.bam`, `/map/SampleName/SampleName.sorted.bam.bai`<br>`/filter/SampleName/SampleName.filtered.bam`<br>`/filter/SampleName/SampleName.filtered.bam.bai`


##### **Command Line Arguments**

Flag                                      | Description
------------------------------------------|-------------------------------------------------------------------------------------------
-h, \-\-help                          | Displays help message
-c <FILE\>, \-\-config <FILE\>      | YAML configuration file with paths to sample data and resource files (and analysis parameters, recommended but optional). Structure follows that of the template YAML file provided in the SLAMDUNKAROO repository. Required.
-a <FILE\>, \-\-adapters <FILE\>    | Adapter FASTA file. Overrides the adapter FASTA specified in the config file. Required.
-l <INT\>, \-\-length <INT\>        | Length of untrimmed reads. Overrides value specified in the config (if applicable). Required.
-i <FILE\>, \-\-index <FILE\>       | Path and basename of bowtie2 index files. Overrides index file path specified in the config (if applicable). Required.
-p <INT\>, \-\-phred <INT\>         | Phred encoding of input FASTQs. Overrides value specified in the config (if applicable). Defaults to phred33 if no value provided in config file or command line.
-t <INT\>, \-\-threads <INT\>       | Number of threads to use/jobs to run at once. If using a machine with multiple cores, should use values >1 to speed up analysis. Defaults to 1.
\-\-dryrun                            | Activates dryrun mode. Good to check for presence of necessary input files, parameters, and dependencies.
\-\-rerun                             | Activates rerun-incomplete mode. This mode only runs rules that did not complete successfully in prior pipeline runs. 

***

# Output

Output files for each step of the pipeline are stored in directories with the same name as the module. The *Detailed Usage* section elaborates on the exact naming scheme of these output files. Within the module directories, results are organized into separate directories for each sample. Where possible, output files are zipped and indexed to compress their size but still allow quick access. Intermediate files (files produced in the middle of a module that are not the final output) are not saved by default, but future features may allow for the toggling of saving intermediate files (i.e. by turning on *debugging* mode).

Additionally, a "configlog" YAML file is produced at the end of an analysis run to serve as a record of the parameters and files used in an analysis run. The naming format of this configlog file is
```
<Date>-<Time>_<ExperimentName>_<Module>.configlog.yaml
```
where


|                    | Format              | Example                                                
|--------------------|---------------------|--------------------------------------------------------
|<Date\>            | Year-Month-Day      | 2000-01-05 *(January 5th, 2000)*
|<Time\>            | Hour.Minute.Second  | 16.45.10 *(analysis finished at 16:45.10 or 4:45.10pm)*
|<ExperimentName\>  | User-provided string| Experiment1-mESC-DMSO-rep-1
|<Module\>          | Module name         | map *(or preproc, filter, all)*

All parameters and their values are explicitly defined, whether they were provided in the original config file by the user, provided in the command line, or set to default values. Date and time of the completion of the analysis is also included in the config log and in the file name (to avoid overwriting old config log files if a new analysis is run in the same directory **[NOT RECOMMENDED - this will override output files unless all old output files are name-changed]**). This configlog file cannot immediately be used as a normal config file (it cannot directly be fed back into the pipeline), but with a few minor changes, it can be modified to be recognized by the pipeline:

1. Remove the `analysis` and `parameters` top-level keys.
2. Outdent the remaining sub-keys (`reference`,`utr`,`numTC`, etc.).
3. Remove the `Date` and `Time` keys and values.
4. Change the `ExperimentName` key to `experiment` (no capitalization, as in the template config file).

The template config file should be used for reference when modifying the configlog file to be recognized by the pipeline. It is recommended to not directly modify the configlog file (since it is supposed to be an accurate record of what analysis was run), but rather copy over relevant information into a new config YAML file if needed.

An example of what a directory might look like after running all of the pipeline would be:

```
project/
|---config.yaml
|---AnalysisDate-AnalysisTime_ExperimentName_ModuleRun.configlog.yaml
|---fastq/
|    |---SampleA/
|    |    |---SampleA_unmapped.fastq.gz
|   ...
|
|---filter/
|    |---SampleA/
|    |    |--- ...
|   ...
|
|---map/
|    |---SampleA/
|    |    |--- ...
|   ...
|
|---preproc/
|    |---SampleA/
|    |    |--- ...
|   ...
|
|---stats/
|    |---SampleA/
|    |    |--- ...
|   ...
|
|---benchmark/
|---logs/

```
The `stats/` directory contains pre- and post-FASTQ processing FASTQC results for each sample. The `benchmark` directory stores text documents with information about each job run such as time elapsed, memory used, etc. This can be useful for debugging or figuring out the resource requirements for each rule, which can then be useful for most effectively distributing the analysis steps on compute nodes or cluster queues (*command line options to be added*). The `logs` directory stores job and cluster error outputs for troubleshooting and recording purposes.

A hidden directory `.snakemake` will also be created in the project directory. This directory stores files necessary for Snakemake to run, as well as the Conda environments created to run certain rules of the pipeline. It should not be necessary to change or check anything within this directory.


***

# Version Info

**Version 0.1 - current**
